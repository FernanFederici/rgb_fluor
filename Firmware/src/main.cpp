#include <Arduino.h>

/********* *********/

// Import required libraries
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FastLED.h>
#include <LittleFS.h>

const char *ssid = "RGB-Fluo 08";

// LED STRIP RGB
#define NUM_LEDS 9 // NUMBER OF LEDS PER STRIP
#define RGB_LED_PIN D5
#define LED_TYPE WS2811
#define COLOR_ORDER GRB

// R, G and B Values
String red_value = "0";
String green_value = "0";
String blue_value = "0";
String intensity_value = "255";

CRGB rgb_led[NUM_LEDS];

const char *PARAM_INPUT = "value"; // GET Request -> LED Server

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);


String processor(const String &var)
{
  Serial.println(var);
  if (var == "REDVALUE")
  {
    return red_value;
  }
  else if (var == "GREENVALUE")
  {
    return String(green_value);
  }
  else if (var == "BLUEVALUE")
  {
    return String(blue_value);
  }
  else if (var == "INTENSITYVALUE")
  {
    return String(intensity_value);
  }
  return String();
}

void setup()
{
  // LED STRIP
  FastLED.addLeds<NEOPIXEL, RGB_LED_PIN>(rgb_led, NUM_LEDS);
  pinMode(RGB_LED_PIN, OUTPUT);

  // Serial port for debugging purposes
  Serial.begin(9600);

  // Initialize LittleFS
  if (!LittleFS.begin())
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  // Print ESP8266 Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send(LittleFS, "/index.html", String(), false, processor); });

  // Route to load style.css file
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send(LittleFS, "/style.css", "text/css"); });

  // Route to set Red Value
  server.on("/redslider", HTTP_GET, [](AsyncWebServerRequest *request)
  {
    String inputMessage;
    if (request->hasParam(PARAM_INPUT))
    {
      inputMessage = request->getParam(PARAM_INPUT)->value();
      red_value = inputMessage;
      for (int i = 0; i < NUM_LEDS;i++)
      {
        rgb_led[i] = CRGB(red_value.toInt(), green_value.toInt(), blue_value.toInt());
        FastLED.show();
        delay(50);
      }
    }
    else
    {
      inputMessage = "No message sent";
    }
    Serial.println(inputMessage);
    request->send(200, "text/plain", "OK");
  });

  // Route to set Green Value
  server.on("/greenslider", HTTP_GET, [](AsyncWebServerRequest *request)
  {
    String inputMessage;
    if (request->hasParam(PARAM_INPUT))
    {
      inputMessage = request->getParam(PARAM_INPUT)->value();
      green_value = inputMessage;
      for (int i = 0; i < NUM_LEDS; i++)
      {
        rgb_led[i] = CRGB(red_value.toInt(), green_value.toInt(), blue_value.toInt());
        FastLED.show();
        delay(50);
      }
    }
    else
    {
      inputMessage = "No message sent";
    }
    Serial.println(inputMessage);
    request->send(200, "text/plain", "OK");
  });

  // Route to set Blue Value
  server.on("/blueslider", HTTP_GET, [](AsyncWebServerRequest *request)
  {
    String inputMessage;
    if (request->hasParam(PARAM_INPUT))
    {
      inputMessage = request->getParam(PARAM_INPUT)->value();
      blue_value = inputMessage;
      Serial.print("R:");
      Serial.println(red_value);
      Serial.print("G:");
      Serial.println(green_value);
      Serial.print("B:");
      Serial.println(blue_value);
      for (int i = 0; i < NUM_LEDS; i++)
      {
        rgb_led[i] = CRGB(red_value.toInt(), green_value.toInt(), blue_value.toInt());
        FastLED.show();
        delay(50);
      }
    }
    else
    {
      inputMessage = "No message sent";
    }
    Serial.println(inputMessage);
    request->send(200, "text/plain", "OK");
  });

  // Route to set Intensity Value
  server.on("/intensityslider", HTTP_GET, [](AsyncWebServerRequest *request)
  {
    String inputMessage;
    if (request->hasParam(PARAM_INPUT))
    {
      inputMessage = request->getParam(PARAM_INPUT)->value();
      intensity_value = inputMessage;

      for (int i = 0; i < NUM_LEDS; i++)
      {
        rgb_led[i] = CRGB(red_value.toInt(), green_value.toInt(), blue_value.toInt());
        FastLED.setBrightness(intensity_value.toInt());
        FastLED.show();
        delay(50);
      }

    }
    else
    {
      inputMessage = "No message sent";
    }
    Serial.println(inputMessage);
    request->send(200, "text/plain", "OK");
  });

  // Start server
  server.begin();
}

void loop()
{}