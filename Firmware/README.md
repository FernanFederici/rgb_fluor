# RGB_Fluo
Desarrollo conjunto con equipo de Lab TecLibre del Instituto de Ingeniería Biológica UC. 
El dispositivo consiste en un instrumento simple para experimentos de fluorecencia. Este proyecto es un desarrollo de bajo costo, abierto de openHardware y openSoftware. El dispositivo permite controlar una fuente de iluminación basado en LEDs RGB a través de Wi-Fi. Por medio de un filtro deslizante permite observar florecencia de reacciones en un grupo de muestras objetivo en distintas bandas.

Todos los archvios necesarios para recrear el dispositivo se encuentran en el siguiente repositorio.

https://gitlab.com/FernanFederici/rgb_fluor



### Características:
* Alimentación central por USB
* 7 LEDs RGB *On board*
* Control remoto vía WiFi de variables: Color e intensidad.
* Compatibilidad con la plataforma de control de la empresa adafruit para LEDs RGBs.

### Dependencias:
* Shield compatible con Tarjeta ESP8266
* LEDs RGB ws2812



## Electronica

Para implementar la electrónica del dispositivo , se diseñó una PCB para controlar un grupo de 7 LEDs RGB. La placa fue diseñada en un formato compatible con el módulo comercial Lolin basado en el microcontrolador ESP8266.

El dispositivo cuenta con un consumo bajo (Aproximadamente 5V, 200mA). Puede ser alimentado por un puerto USB o conectado a un cargador genérico ( como los que suelen usarse en los smartphones).


## Firmware and software
El código cargado en la ESP8266 usa un software acces point via WiFi y cuenta con un servidor local montado en la placa, el cual puede ser accesado desde cualquier dispositivo conectado en la misma red. Una aplicación web (desarrollada en HTML y CCS) esta hosteada en el servidor, esta permite controlar tanto la intencidad como el color del grupo de LEDs disponibles.

### Instrucciones

Cuando el dispositivo es alimentado, el microcontrolador genera una red local con un nombre específico, el cual puede ser editado por firmware (por defecto RGB-Fluo 0x).
La red generada puede ser encontrada por cualquier disposivito con la habilidad de conectarse via WiFi. ej: PC, tablets, smartphones y no posee contraseña.

Una vez concetado a la red, podemos acceder a la aplicación entrando a la siguiente dirección ip: http://192.168.4.1/



La aplicación web cuenta con controladores tipo sliders para cada uno de los canales RGB y la intensidad.


# Hardware

## Electronica

The designed PCB is compatible with the commercial NodeMCU v3 "Lolin" board based on the ESP8266 microcontroller. The PCB has a shield format and has 9 digital controlled WS2812B RGB LEDs. 

The design was carried out using the open-source software KiCAD 5. For the reported development, 30 PCBs were manufactured in the JLC company and the components were acquired through the LCSC company.

The gerber files to replicate the board are available in the repository under the path PCB/Gerber_Files

Below,  are images of the 3D models of the designed PCB, the schematic and the BOM quoted in the LCSC company.






Item
Name
Quantity
Part number LCSC
Note
Capacitor
C1
1
C77386
MLCC SMD 0603 1uF.
RGB LED
D1-D9
9
C114586
WS2812B
Header
J1
2
C2897378
FH, 1 row x 15 pos. pitch 2.54.




The board is simple to assemble and the components used can be assembled by hand soldering.

## Firmware and software
The firmware was developed on platformIO  with the Arduino framework. It implements a local software access point and mounts a local server on the ESP8266 board. The server hosts a web page developed in HTML and CSS.

The web page developed is simple and allows, by horizontal slider-type controls, to control the RGB components of the color of the LEDs, each one represented by an integer between 0 and 255; and the value of the intensity of the LEDs.

The code uses the following libraries to work (all of them available from the official Arduino library manager)

ESP8266 WiFi
me-no-dev/ESPAsyncTCP
me-no-dev/ESPAsyncWebServer
LittleFS
fastled/FastLED

The ESP8266WiFi, ESPAsyncTCP and ESP Async WebServer libraries are used to manage WiFi communication, implement the access point and set up the local server. The LittleFS library implements a memory management (File system) for the microcontroller, allowing the html and css files from the web page to be stored in the flash memory of the board.
 
Finally, the FastLED library allows you to control RGB LEDs. These LEDs can be controlled individually, however for the purposes of this application they are all controlled at the same time.



## Compiling the code

To upload the code to the device, some preliminary steps are necessary. You have to mount an image in the memory of the microcontroller with the files index.html and syles.ccs that contain the web page found in the path

./firmware/data

You have to run the commands.

pio runs -t buildfs
pio runs -t uploadfs

Once the image is uploaded we can load the firmware through the commands

Compile: run pio
Load: pio run --target load

