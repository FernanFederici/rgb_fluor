# RGB_fluor

En este repositorio encontrarás todos los archivos y documentación para fabricar un dispositivo de docencia de bajo costo y Free/Libre Open Source llamado RGB fluor. Este dispositivo fue diseñado para realizar experimentos fluorecencia.

## Licencias

Todos los diseños realizados en este proyecto son de libre acceso y se encuentran a disposición para su descarga, reproducción, uso y comercialización bajo las siguientes licencias. 

| Diseño    |  Licencia |
|--------   |----------|
| Diseño 3D |  CERN-OHL-S-2.0 |
| PCB | CERN-OHL-S-2.0 |
| Firmware y software | GPL-3.0-or-later |
| Documentación  | CC-BY-SA-4.0  |





## Descripción del hardware

El dispositivo permite controlar una fuente de luz RGB via Wifi y cuenta en su estructura con un filtro deslizante que permite observar fluoreciencia en las muestras. 

El dispositivo se compone de una estructura impresa en 3D, una serie de filtros, una PCB con LEDs RGB y una tarjeta ESP8266 NodeMCU. 

El diseño del dispositivo ha sido realizado con el software libre OpenScad. En la carpeta [openscad](https://gitlab.com/FernanFederici/rgb_fluor/-/tree/main/openscad) puedes encontrar el archivo fuente del diseño y algunas indicaciones acerca de los parámetros utilizados. 

<img src="/fotos/Sin_ti%CC%81tulo.png" width="500"/>

Los filtros utilizados son de bajo costo (leefilters), los cuales han sido usado con éxito en prácticos con [GMOdetective](https://github.com/MakerLabCRI/GMODetective-Detector). 
Usamos [105](http://www.leefilters.com/lighting/colour-details.html#105), 
[113](http://www.leefilters.com/lighting/colour-details.html#113&filter=cf), y [122](http://www.leefilters.com/lighting/colour-details.html#122 ).

Para la iluminación se desarrolló una PCB shield que cuenta con 9 LEDs WS2812b, conocidos como [neoPixels](https://www.adafruit.com/category/168). La PCB fue desarrollada en el software libre KiCAD. En la carpeta [PCB](https://gitlab.com/FernanFederici/rgb_fluor/-/tree/main/PCB) puedes encontrar los archivos fuente del diseño. 

Los materiales biológicos que se usan en nuestro [práctico](https://docs.google.com/document/d/17OYUNyA4ynrUG6PlBgAHzUIgEy-NbYp767M4pV4rm14/edit) pueden ser obtenidos desde Addgene (link pronto) o directamente desde nuestro [laboratorio](https://federicilab.org/) .


<details><summary> Características</summary>

* Filtros seleccionables.
* Capacidad soporte para 5 tubos para microcentrífuga.  
* Alimentación central por USB (5V/300mA).
* 9 LEDs RGB *On board*
* Control remoto vía WiFi de variables: Color e intensidad.
* Compatibilidad con la plataforma de control de la empresa adafruit para LEDs RGBs.

</details>

<details><summary>Piezas 3D</summary>

pronto info de cada pieza de la carpeta STL
</details>

<details><summary>Modos de uso</summary>

Filtro rojo y luz verde
<img src="/fotos/FiltroRojo.png" width="400"/>

Filtro Naranja y luz azul
<img src="/fotos/filtroOrange.png" width="400"/>


</details>

---

### Electrónica

Las siguietes figuras muestran un detalle de la visualización 3D de la placa desarrollada y el esquemático. La placa fue desarrollada en KiCAD, y los archivos fuentes se encuentran en la carpeta [PCB](https://gitlab.com/FernanFederici/rgb_fluor/-/tree/main/PCB).

<img src="/fotos/RGB_fluo.png" width="400"/>
<img src="/fotos/schematic.svg" width="400" style="transform:rotate(90deg);"/>


El dispositivo consume baja potencia (Aproximadamente 5V/300mA) y puede ser alimentado con un cargador generíco USB o directamente por un puerto USB de un computador. 

Los componentes utilizados para la electrónica se enlistan en el siguiente BOM

|Item 	| Nombre | Cantidad | PN LCSC | Comentario  |
|-------|--------|----------|---------|-------------|
|Capacitor| C1	| 1 | C77386 | MLCC SMD 0603 1uf 	|
|LED RGB| D1-D9	| 9 | C114586 | WS2812B 	|
| Headers| J1	| 2 | C2897378 | FH, 1row x 15pos. p2.54 	|
| NodeMCU| -	| 1 | C2764073 | NodeMCU ESP8266 	|


Esta placa desarrollada es posee componentes SMD pero los componentes son simples de montar y pueden ser montados por medio de soldadura a mano. 

<hr/>

### Firmware y software

El firmware fue desarrollado en platformIO  con el framework de Arduino. Es un código simple que implementa un punto de acceso y un servidor local montado en la tarjeta ESP8266. El servidor local hostea una pagina sencilla desarollada en HTML y CCS. Los archivos fuentes pueden encontrarse en la carpeta [Firmware](https://gitlab.com/FernanFederici/rgb_fluor/-/tree/main/Firmware) de este repositorio.

La página permite controlar los RGB por medio de sliders horizontales. Cada uno controla las componentes RGB y la intensidad de los LEDs. 

<img src="/fotos/app.png" width="400"/>

#### Bibliotecas
El código usa las siguientes bibliotecas disponibles desde el repositorio oficial de Arduino.

* ESP8266 WiFi
* me-no-dev/ESPAsyncTCP
* me-no-dev/ESPAsyncWebServer
* LittleFS
* fastled/FastLED

Las bibliotecas ESP8266WiFi, ESPAsyncTCP  y ESP Async WebServer  implementan la comunicación Wifi. La biblioteca  LittleFS implementa una memoria que pertie almacenar los archivos  html and css de la página web en la memoria flash del microcontrolador.
 
La biblioteca FastLED permite controlar los LEDs RGB. Estos pueden ser controlados de forma individual, sin embargo para propositos de este dispositivo, la aplicación los controla a todos a la vez.

#### Compilando el código

Para cargar el código a un dispositivo nuevo se necesitan algunos pasos adicionales. Primero es necesario crear la imagen de la memoria en el microcontrolador y cargarle los archivos index.html and syles.ccs  que contienen la pagina web. Estos se encuentran en el siguiente path

```
./firmware/data
```

Esto se puede implementar por medio de los comandos

```
pio runs -t buildfs
pio runs -t uploadfs
```

Una vez cargados los archivos en la flash podemos compilar el código de forma normal o pormedio de los compandos: 

```
#Compilar: 
pio run
#Cargar código
pio run --target load
```

### Instrucciones de uso 

1. Para utilizar el dispositivo es necesario tener el código cargado y el dispositivo alimentado. Una vez encendido el dispositvo es necesario entrar a la red wifi levantada  utilizando un computador o un smartphone. Esta debiese tener las siguientes credenciales. 

```
#WIFI SSID 
RGB-Fluo 1.2
```


2. Estando conectados a la red debemos entrar a la dirección ip preestablecida:

```
192.168.4.1
```

3. Esta dirección implementa página web con los controladores. En ella podemos seleccionar las componentes  de R,G y B de los LEDs y su intensisda. Ojo: los LEDs solo se prenderán si la intensidad es distinta de 0.

<hr/>

Proyecto financiado por **Fondecyt Regular 1211218** y el Instituto Milenio iBio.

Comentarios? 
Podes consultar los issues para saber en qué cosas estamos trabajando para mejorar este diseño o dejar tus sugerencias.
