/*
This is part of a collaborative project with IOWlabs
This file creates a 3D printed case for the RGB fluo board
All this code is licensed under the Creative Commons - Attribution  (CC BY 3.0)
Fernán Federici 2020
*/
         
       
//--------------------------parameter to play with
dx_col=25.7; //distance between pillars in x
dy_col=52.3; //distance between pillars in y
pcb_x= 32;
pcb_y=80;
btw_pcb_h=18;//distance between pcbs
h_pillar=5;   //height of pillar holding first PCB
d_holes=25.5-(3*2)-2;
space=2;
int_x= pcb_y*0.7;//internal void space
int_y=pcb_y+10 +space; //internal void space
wall=2;
win_h=pcb_x+wall*4; //window space in z axis 
case_height=80;
corr=0.2;// used for printing imperfections
screw_in_r=2.5/2;
tube_r=3/2+2;   //radius of pillar holding PCB
base_plat=2;
h_int_pillar=h_pillar+btw_pcb_h/3;
cart_depth=h_int_pillar*2.5;//how deep PCB and tube holder are
push_joint_h=10;
push_joint_x=int_x + wall*2;
push_joint_y=int_y + wall*2;
pcr_r=6.3/2;               
h_reinf=3;       
$fn=60;
d_tubes=9; //distance between tubes      
num_tubes=8; //number of PCR tubes               
base_h=win_h*1.4;            
win_PCR_h=win_h/2; //height PCR tube holder
pcr_holder_y=int_y-wall;      //the wall substraction is to favour the easy entry    
filter_holder_y=push_joint_y-wall;//wall at the end when pushing in
filter_holder_x=push_joint_x-wall*2;
filter_lid_holder_x=push_joint_x/2;    
window_filter_y=filter_holder_y/3;
top_opening_h= wall*2+1+wall;            
filter_xy=45;             
visor_xy=  filter_xy-wall*8;                
color_vec = ["black","red","cyan","green","pink","purple", "white"];
space_btn_filters=filter_xy+wall*2;


//
 translate([ 0.00, -120.00,case_height*1.65-wall]) RGB_filter_holder_bt(filters=3);
translate([ 0.00, 0.00,case_height/1.5])RGB_case_top();
 
RGB_case_bottom();
  translate([push_joint_x+wall*5, 0, win_h/2+wall])  pcr_holder_hol();   
translate([push_joint_x, 0, win_h/2+wall])  pcr_holder();       
translate([ -40.00, 0.00,win_h/2+wall*1.5]) rotate([0,90,0]) PCB_holder();//el height is win_h       
  translate([ 0.00,-120.00,case_height*1.65-wall*2]) RGB_filter_holder_top(filters=3);        
translate([ -65.00, -90.00,win_h/2+wall*1.5]) pcb_lid();//el height is win_h, la distancia de notches es wallx2 



module RGB_filter_holder_bt(filters){
   if (filters==1){   difference(){
    translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x-wall, filter_holder_y, wall*2 ], center=true);  };
 //notch lid lee filter
                     translate([ 0, 0, -1/2 ])  color("red")
                   cube([ filter_xy,filter_xy,  wall ], center=true); 
   translate([ 0, 0, -1/2 ])  color("blue")
// opening lid
                     translate([ filter_lid_holder_x/2-wall*2, -push_joint_x/2+wall*2,wall]) cube([  filter_lid_holder_x*20, filter_holder_y*20,  wall*2 ], center=true);       
 //view window                                   
   translate([ 0, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ visor_xy, visor_xy,  100 ], center=true);
}}} 
 else { 
     difference(){
     translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x-wall, (filter_xy+wall*2)*filters, wall*2 ], center=true);  };
                     //notch lid lee filter                
   for (i = [1: filters])
     translate([ 0,-(filters*space_btn_filters/2)-space_btn_filters/2+space_btn_filters*i,0 ]) color(color_vec[i])   cube([ filter_xy,filter_xy,  wall ], center=true); 
// opening lid
   translate([ 0, 0, -1/2 ])  color("blue")
                     translate([ filter_lid_holder_x/2-wall*2, -push_joint_x/2+wall*2, wall]) cube([  filter_lid_holder_x*20, (filter_xy+wall*2)*filters*20,  wall*2 ], center=true); 
 //view window    
       for (i = [1: filters])
     translate([ 0,-(filters*space_btn_filters/2)-space_btn_filters/2+space_btn_filters*i,0 ]) color(color_vec[i])   color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ visor_xy, visor_xy,  100 ], center=true);      
    }}}}
    
module RGB_filter_holder_top(filters){
   if (filters==1){   difference(){
    translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x-wall, filter_holder_y, wall*2 ], center=true);  };

   translate([ 0, 0, -1/2 ])  color("blue")
// opening lid
                     translate([ filter_lid_holder_x/2-wall*2, -push_joint_x/2+wall*2,wall]) cube([  filter_lid_holder_x*20, filter_holder_y*20,  wall*2 ], center=true);       
 //view window                                   
   translate([ 0, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ visor_xy, visor_xy,  100 ], center=true);
}}} 
 else { 
     difference(){
     translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x-wall, (filter_xy+wall*2)*filters, wall*2 ], center=true);  };
// opening lid
   translate([ 0, 0, -1/2 ])  color("blue")
                     translate([ filter_lid_holder_x/2-wall*2, -push_joint_x/2+wall*2,wall]) cube([  filter_lid_holder_x*20, filter_holder_y*20,  wall*2 ], center=true); 
//view window   
       for (i = [1: filters])
     translate([ 0,-(filters*space_btn_filters/2)-space_btn_filters/2+space_btn_filters*i,0 ]) color(color_vec[i+1])  minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ visor_xy, visor_xy,  100 ], center=true);      
         
// //view window                                   
//   translate([ 0, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
//                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
//                    cube([ visor_xy, visor_xy,  100 ], center=true);
    }}
   }}
    
 

module RGB_case_top(){
  translate([ 0.00, 0.00,case_height/2]){
      difference(){
   translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=case_height, center=true); 
                     cube([ push_joint_x+wall*2, push_joint_y+wall*2, 1 ], center=true);  };
  //joint push in
   translate([ 0, 0, -case_height/2-wall+push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall , center=true); 
                    cube([ push_joint_x-wall+corr, push_joint_y-wall+corr,  1 ], center=true); 
        }             
   translate([ 0, 0, -wall*5.5 ])  color("red")minkowski(){ 
                    cylinder(r=wall,h=case_height+corr , center=true); 
                    cube([ push_joint_x-wall*2, push_joint_y-wall*2,  1 ], center=true); 
        } 
//space for filter holder
         translate([0,-wall*2,case_height/2- wall*3]) color("yellow") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x+corr*3, filter_holder_y+wall*20, wall*2+corr*3 ], center=true);  };   
  //top opening             
   translate([0,0,case_height/2-top_opening_h/2+wall/2]) color("blue") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x-wall*2, filter_holder_y-wall*4,top_opening_h ], center=true);  };          
  //visor
                      //view window                                   
   translate([ 0, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ visor_xy, visor_xy,  100 ], center=true);
};
                    }}
                }

  
module RGB_case_bottom(){
 difference(){   
 //Outer      
                    union(){
 //top joint
             translate([ 0,0, base_h+push_joint_h/2-wall ])  color("white")  minkowski(){ //smaller with corr*2 to fit the top  
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_x-wall-corr*2, push_joint_y-wall-corr*2,  1 ], center=true);};
// base
             translate([ 0.00, 0.00, base_h/2])   color("red")   minkowski(){ 
                    cylinder(r=wall,h=wall, center=true); 
                    cube([ push_joint_x+wall*2, push_joint_y+wall*2,  base_h ], center=true);
                         };              
 // base piso
//                     translate([ 0.00, 0.00, 0])  color("green")    minkowski(){ 
//                    cylinder(r=wall,h=base_plat, center=true); 
//                    cube([ push_joint_x*1.4, push_joint_y*1.4,  wall ], center=true);                
//            }
            }
//internal part
              translate([ 0.00, 0.00, win_h+ wall]) color("yellow")    minkowski(){ 
                    cylinder(r=wall,h=win_h*2, center=true); 
                    cube([ push_joint_x-wall*5, push_joint_y-wall*5,  1 ], center=true);
                         };
//window pcb (4 corr)
           translate([-win_h/2,0, win_h/2+wall*1.5])  color("green")  neg_pcb_window(); 
             
//window PCR holder
           translate([win_h/2,0, win_h/2+wall])  color("cyan")  neg_pcr_window();         
            }
            //techo    
             difference(){
                 translate([-push_joint_x/2+cart_depth/2-wall,0, win_h/2+wall*2])  color("blue")  cube([ cart_depth-wall,int_y, win_h+wall*2 ], center=true);  
                    translate([-push_joint_x/4,0, win_h/2+wall*1.5])  color("white")  neg_pcb_window();                
             }
            //pillars for holding the pcb 
             //  translate([ 0.00, 0.00, -h_pillar/2 ])     four_pillars(); 
            }  
            
     
 module pcr_holder_hol(){
        difference(){  
           
         translate([wall*3, 0, 0])  color("green")   cube([ cart_depth-wall,pcr_holder_y, win_PCR_h-wall*2], center=true);     
          translate([wall*6, 0, 0])  color("green")   cube([ cart_depth,pcr_holder_y, win_PCR_h-wall*6], center=true);   
            //external slope
         translate([wall*2, 0, win_PCR_h/2+wall/1.5+corr*3]) rotate([0,-4,0]) color("green")   cube([ cart_depth,pcr_holder_y, win_PCR_h-wall*6], center=true);  translate([wall*2, 0, -win_PCR_h/2-wall/1.5-corr*3]) rotate([0,4,0]) color("red")   cube([ cart_depth,pcr_holder_y, win_PCR_h-wall*6], center=true);    
           //internal
            # translate([0, 0, 0])  rotate([90,0,0]) color("green")   cylinder(r=(win_PCR_h-wall*6)/2, h=pcr_holder_y, center=true);    
//           //internal slope
//               #    translate([wall*7, 0, win_PCR_h/4-wall*1.5]) rotate([0,-4,0]) color("green")   cube([ cart_depth,pcr_holder_y, win_PCR_h/4], center=true);      
//            #    translate([wall*7, 0, -win_PCR_h/4+wall*1.5]) rotate([0,4,0]) color("green")   cube([ cart_depth,pcr_holder_y, win_PCR_h/4], center=true);        
        }                            
                }   
            

 module pcr_holder(){
        difference(){  
            color("yellow")   cube([ cart_depth,pcr_holder_y, win_PCR_h-corr*2], center=true);     
         translate([wall*3, 0, 0])  color("green")   cube([ cart_depth,pcr_holder_y, win_PCR_h-wall*2], center=true);     
           translate([-d_tubes,-(d_tubes*(num_tubes-1))/2,0 ]) rotate([0,90,0])    eighttubes();
        }
        //notches
             translate([cart_depth/4, 0, win_PCR_h/2+wall/2])  color("cyan")  cube([ cart_depth/2, pcr_holder_y-corr*2,  wall*1.5 ], center=true);       
           translate([cart_depth/4, 0, -win_PCR_h/2-wall/2])  color("white")  cube([ cart_depth/2, pcr_holder_y-corr*2,  wall*1.5 ], center=true);                                
                }   
    
module neg_pcb_window(){
    cube([ push_joint_x/2+wall,int_y+corr*3, win_h+corr*4 ], center=true);     
    }       
module neg_pcr_window(){
cube([ cart_depth+corr*3,pcr_holder_y+corr*5, win_PCR_h+corr ], center=true);     
    }       
  

module PCB_holder(){
       union(){   
           difference(){  
               translate([0,0, -cart_depth/2]) color("green")  cube([ win_h, int_y,  cart_depth ], center=true);
               //interior must accommodate pcb width (x)
              translate([0,0, -cart_depth/2-wall*1.5]) color("red")  cube([ win_h-wall*2, int_y,  cart_depth ], center=true);
            //LED window
           #translate([0,0, -3.7]) color("cyan") rotate([90,0,0]) cylinder(r=4, h=pcb_y, center=true);//cube([ pcr_r*2, pcb_y,  cart_depth *2], center=true);;
        }
                     translate([0, 0,-h_int_pillar/2 -wall ])   difference(){  
  fourReinforcement2(); 
                          four_int_pillars(); }                  
                       //notches for lid
           translate([win_h/2-wall-wall/2,0, -cart_depth+wall/2])   color("blue")  cube([ wall, int_y-corr*2,  wall ], center=true);   
            translate([win_h/2-wall-wall/2,0, -cart_depth+wall*2+wall/2])   color("cyan")  cube([ wall, int_y-corr*2,  wall ], center=true);       
                 translate([-win_h/2+wall+wall/2,0, -cart_depth+wall/2])   color("blue")  cube([ wall, int_y-corr*2,  wall ], center=true);   
            translate([-win_h/2+wall+wall/2,0, -cart_depth+wall*2+wall/2])   color("cyan")  cube([ wall, int_y-corr*2,  wall ], center=true);                
                    
                          //  linear_extrude(height = 5, center = true, convexity = 10, twist = 0, slices = 20, scale = 1.0) { circle(r=2);}
     /*old pillar version, it doesnt work due to the weakness of PLA pilars 
             translate([0, 0,-h_int_pillar/2 -wall ])   union(){    four_int_pillars();
  fourReinforcement();  }*/
                } }  
                
            
module pcb_lid(){
difference(){
    union(){
        cube([ wall-corr*4, int_y-corr*2,  win_h-wall*2-corr*4 ], center=true);   
        translate([ -wall/2+15/2, -int_y/2-wall/2,0]) cube([ 15, wall*2,  win_h-wall*2-corr*4 ], center=true);       
    }
#translate([ -wall/2-15/2+15+wall, -int_y/2-wall/2,0])  cube([ 15, wall*2,  win_h/2-wall*2 ], center=true);}
    }                       
                
module eighttubes(){
         #for ( i = [0 : num_tubes-1] ){
    translate([0, i*9, 0])
   pcr_tube();
}
                }     
                        
module eighttubes(){
         #for ( i = [0 : num_tubes-1] ){
    translate([0, i*9, 0])
   pcr_tube();
}
                }     
              

                //module for 4 tubes               
module fourtubes(){
       color("blue")  pcr_tube();      
            color("green") translate([pcr_r*2+pcr_r/2, -pcr_r*2+pcr_r/2,0 ])  pcr_tube();  
            color("red") translate([0, pcr_r*2+pcr_r*2-pcr_r,0 ])  pcr_tube();      
        color("cyan") translate([pcr_r*2+pcr_r/2, pcr_r*2-pcr_r/2,0 ])  pcr_tube();     
                }    
//          fourReinforcement2();      //this is reinforcement for receiving pillars as negative
      module fourReinforcement2(){
       color("blue") translate([dx_col/2, dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf2();      
            color("green") translate([dx_col/2, -dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf2();  
            color("red") translate([-dx_col/2, dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf2();      
        color("cyan") translate([-dx_col/2, -dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf2();     
                }    
//          fourReinforcement();      
      module fourReinforcement(){
       color("blue") translate([dx_col/2, dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf();      
            color("green") translate([dx_col/2, -dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf();  
            color("red") translate([-dx_col/2, dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf();      
        color("cyan") translate([-dx_col/2, -dy_col/2,h_int_pillar/2-h_reinf/2 ]) leg_reinf();     
                }                   
                
 //           four_int_pillars(); 
 module four_int_pillars(){
        color("blue") translate([dx_col/2, dy_col/2,0 ]) pillar_int();      
            color("green") translate([dx_col/2, -dy_col/2,0 ])  pillar_int();   
            color("red") translate([-dx_col/2, dy_col/2,0 ])  pillar_int();      
        color("cyan") translate([-dx_col/2, -dy_col/2,0 ])  pillar_int();   
                }                  
//           four_pillars(); 
 module four_pillars(){
        color("blue") translate([dx_col/2, dy_col/2,0 ]) pillar();      
            color("green") translate([dx_col/2, -dy_col/2,0 ])  pillar();   
            color("red") translate([-dx_col/2, dy_col/2,0 ])  pillar();      
        color("cyan") translate([-dx_col/2, -dy_col/2,0 ])  pillar();   
                }     
module pillar(){
        difference(){  
            cylinder(r=tube_r, h=h_pillar, center=true); 
            cylinder(r=screw_in_r+corr, h=h_pillar+corr, center=true);
        }
                }     
                
module pillar_int(){
             cylinder(r=screw_in_r+corr, h=h_int_pillar, center=true);
                }    
          
module pcr_tube(){
             cylinder(r=pcr_r+corr, h=h_int_pillar, center=true);
                }    
module leg_reinf2(){
             cylinder(r1=screw_in_r, r2= pcr_r*1.5, h=h_reinf*2, center=true);
                }    
                
module leg_reinf(){
             cylinder(r1=screw_in_r, r2= pcr_r, h=h_reinf, center=true);
                }    
                                     
             //old useful modules
                /*  deprecated stuff 
     
     module PCB_holder_v0(){//el ancho es win_h+2walls de los techitos
       union(){   difference(){  
            color("green")  cube([ win_h, int_y-corr*2,  wall ], center=true);
            //LED window
            color("red")  cube([ pcr_r*2, pcb_y,  wall *2], center=true);;
        }
         translate([win_h/2+wall/2,0, -win_h/2/2+wall/2]) rotate([0,90,0])    color("white")  cube([ win_h/2, int_y-corr*2,  wall ], center=true);
          translate([-(win_h/2+wall/2),0, -win_h/2/2+wall/2]) rotate([0,90,0])    color("yellow")  cube([ win_h/2, int_y-corr*2,  wall ], center=true);
             translate([0, 0,-h_int_pillar/2 ])       four_int_pillars();
  translate([0, 0,-h_int_pillar/2 ]) fourReinforcement();  
                } }  
     
     
 

RGB_case_bottom();
module RGB_case_bottom(){
 difference(){   
                    //Outer      
                    union(){
                   //top joint
             translate([ 0,0, push_joint_h/2-wall ])  color("white")  minkowski(){ //smaller with corr*2 to fit the top cone 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_x-wall-corr*2, push_joint_y-wall-corr*2,  1 ], center=true);};
                    // base
             translate([ 0.00, 0.00, -wall*2])   color("red")   minkowski(){ 
                    cylinder(r=wall,h=wall, center=true); 
                    cube([ push_joint_x, push_joint_y,  wall*4 ], center=true);
                         };
                    
                        // base piso
                     translate([ 0.00, 0.00, -wall*4-1])  color("green")    minkowski(){ 
                    cylinder(r=wall,h=base_plat, center=true); 
                    cube([ push_joint_x*1.4, push_joint_y*1.4,  2 ], center=true);
                          
            }}
            //internal part
            
              translate([ 0.00, 0.00, -wall*2+wall*4]) color("yellow")    minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                    cube([ push_joint_x-wall*4, push_joint_y-wall*4,  wall*4+push_joint_h ], center=true);
                         };
              //cable
                  translate([ 0.00, pcb_y/2,-wall/2])  cube([ 10,20, 8 ], center=true);
;       
            }
            //pillars for holding the pcb 
               translate([ 0.00, 0.00, -h_pillar/2 ])     four_pillars(); 
            }       
//translate([ 0.00, -100.00,case_height*1.6]) RGB_filter_holder_v0();
module RGB_filter_holder_v0(){
      difference(){
    translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x-wall, filter_holder_y, wall*2 ], center=true);  };
   translate([ 0, 0, -1/2 ])  color("red")
//notch lid lee filter
                    translate([ filter_lid_holder_x/2-wall*2, -push_joint_x/2+wall*5,0]) cube([  filter_lid_holder_x+wall*2, filter_holder_y,  wall ], center=true); 
   translate([ 0, 0, -1/2 ])  color("blue")
// opening lid
                     translate([ filter_lid_holder_x/2-wall*2, -push_joint_x/2+wall*2,wall]) cube([  filter_lid_holder_x, filter_holder_y,  wall*2 ], center=true);       
 //view window                                   
   translate([ push_joint_x/5, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_x-wall*20, filter_holder_y-wall*30,  100 ], center=true);
}}}  

// translate([ 0, -0, 0 ]) RGB_filter_lid();            
module RGB_filter_lid(){
      difference(){
   union(){
   translate([ filter_lid_holder_x/2-wall*2, 0, -1/2 ])  color("red")
//notch lid lee filter
                      translate([0, -push_joint_x/2+wall*2.5,0]) cube([ filter_lid_holder_x+wall, filter_holder_y,  wall/2 ], center=true); 

// opening lid
                    translate([filter_lid_holder_x/2-wall*2,-((filter_holder_y+wall*2)-(window_filter_y+wall*2))/2/2+wall,wall/2]) color("cyan") cube([ filter_lid_holder_x-corr*3,(filter_holder_y+wall*2)-(window_filter_y+wall*2)+wall*4,  wall ], center=true);   //wall*2 because of minkowski calculation added in    
                 }
 //view window                                   
   translate([  push_joint_x/5, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ push_joint_x-wall*20,window_filter_y,  100 ], center=true);
                 } 
                     }}
                     
                         module lineup(filters, space) {
     color_vec = ["black","red","cyan","green","pink","purple"];
   for (i = [1: filters])
     translate([ 0,-(filters*space/2)-space/2+space*i,0 ]) color(color_vec[i]) children(0);
     lineup(filters, space) {sphere()}
     
        

//translate([ 0.00, 0.00,10]) rotate([180,0,0]) RGB_filter_holder_tp();
module RGB_filter_holder_tp(){
      difference(){
    translate([0,0,-0.5]) color("green") minkowski(){ 
                    cylinder(r=wall,h=1, center=true); 
                     cube([ filter_holder_x-wall, filter_holder_y, wall*2 ], center=true);  };
  translate([ 0, 0, -1/2 ])  color("blue")
                     translate([ filter_lid_holder_x/2-wall*2, -push_joint_x/2+wall*2,wall]) cube([  filter_lid_holder_x*20, filter_holder_y*20,  wall*2 ], center=true);       
 //view window                                   
   translate([ 0, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ visor_xy, visor_xy,  100 ], center=true);
}} difference() { cube([ filter_xy-corr*4,filter_xy-corr*4,  wall ], center=true);
       translate([ 0, 0, -push_joint_h/2 ])  color("white")minkowski(){ 
                    cylinder(r=wall,h=push_joint_h-wall*2 , center=true); 
                    cube([ visor_xy, visor_xy,  100 ], center=true);
    }
}}  


*/                  